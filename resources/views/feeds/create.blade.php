@extends('app')

@section('title')
    Create a new Feed
@stop

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Create a new Feed</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('errors.error_list')
                        {!! Form::model($feed = new \App\Feed(), ['url' => 'feeds', 'role' => 'form',
                                                                    'files' => true]) !!}
                        @include('feeds.form', ['submitButtonText' => 'Create'])
                        {!! Form::close() !!}
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->
@stop