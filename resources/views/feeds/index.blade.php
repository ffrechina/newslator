@extends('app')

@section('title')
    Newslator
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Newslator
                <small>{{ \Carbon\Carbon::today()->format('d-m-Y') }}</small>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    @if(count($feeds) == 0)
        <p>There's no feeds</p>
    @else
        @foreach ($feeds as $feed)
            <!-- Feed BLock -->
            <div class="row feed-block">
                <div class="col-md-7">
                    <img class="img-responsive" src="{{ $feed->image->url('large') }}" alt="">
                </div>
                <!-- /.col-md-7 -->
                <div class="col-md-5">
                    <h4 class="grey">{{ $feed->publisher }}</h4>
                    <h3>{{ $feed->title }}</h3>
                    <h5>Created {{ $feed->created_at->diffForHumans() }}</h5>
                    <p>{{ $feed->body }}</p>
                    <a class="btn btn-primary" href="{{ $feed->source }}">View Article
                        <span class="glyphicon glyphicon-chevron-right"></span></a>
                </div>
                <!-- /.col-md-5 -->
            </div>
            <!-- /.row feed-block -->
            <hr />
        @endforeach
        {!! $feeds->links() !!}
    @endif
@stop

@section('footer')
    <hr />
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Francisco Frechina</p>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </footer>
    <!-- /footer -->
@stop