@extends('app')

@section('title')
    Update a Feed
@stop

@section('content')
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Update a Feed</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('errors.error_list')
                        {!! Form::model($feed, ['method' => 'PATCH', 'url' => 'feeds/' . $feed->id, 'role' => 'form',
                                                'files' => true]) !!}
                        @include('feeds.form', ['submitButtonText' => 'Update'])
                        {!! Form::close() !!}
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
@stop