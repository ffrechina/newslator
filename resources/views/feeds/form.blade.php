<div class="row">
    <div class="col-xs-12">
        <!-- Publisher Form Input -->
        <div class="form-group">
            {!! Form::label('publisher', 'Publisher') !!}
            {!! Form::text('publisher', null, ['class' => 'form-control',
                                    'placeholder' => 'Enter the feed\'s publisher (Required)']) !!}
        </div>
        <!-- /.form-group -->
        <!-- Title Form Input -->
        <div class="form-group">
            {!! Form::label('title', 'Title') !!}
            {!! Form::text('title', null, ['class' => 'form-control',
                                    'placeholder' => 'Enter the feed\'s title (Required)']) !!}
        </div>
        <!-- /.form-group -->
        <!-- Body Form Input -->
        <div class="form-group">
            {!! Form::label('body', 'Body') !!}
            {!! Form::textarea('body', null, ['class' => 'form-control',
                                    'rows' => 4,
                                    'placeholder' => 'Enter the feed\'s body (Required)']) !!}
        </div>
        <!-- /.form-group -->
        <!-- Image Form Upload -->
        <div class="form-group">
            {!! Form::label('image', 'Image') !!}
            {!! Form::file('image') !!}
        </div>
        <!-- /.form-group -->
        <!-- Source Form Input -->
        <div class="form-group">
            {!! Form::label('source', 'Source') !!}
            {!! Form::text('source', null, ['class' => 'form-control',
                                    'placeholder' => 'Enter the feed\'s source (Optional)']) !!}
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-xs-12 -->
    <div class="col-xs-12">
        {!! Form::submit($submitButtonText,['class' => 'btn btn-primary btn-lg btn-block']) !!}
        {!! link_to(URL::previous(), 'Cancel', ['class' => 'btn btn-default btn-lg btn-block']) !!}
    </div>
    <!-- /.col-xs-12 -->
</div>
<!-- /.row -->