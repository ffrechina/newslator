@extends('app')

@section('title')
    Show a Feed
@stop

@section('content')
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Show a Feed</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-7">
                                <img class="img-responsive" src="{{ $feed->image->url('large') }}" alt="">
                            </div>
                            <!-- /.col-md-7 -->
                            <div class="col-md-5">
                                <h4 class="grey">{{ $feed->publisher }}</h4>
                                <h3>{{ $feed->title }}</h3>
                                <h5>Created {{ $feed->created_at->diffForHumans() }}</h5>
                                <p>{{ $feed->body }}</p>
                                <a class="btn btn-primary" href="{{ $feed->source }}">View Article
                                    <span class="glyphicon glyphicon-chevron-right"></span></a>
                            </div>
                            <!-- /.col-md-5 -->
                        </div>
                        <!-- /.row -->
                        <hr />
                        {!! link_to(url('feeds/'.$feed->id.'/edit'), 'Edit feed', ['class' => 'btn btn-primary
                                                                                    btn-lg btn-block']) !!}
                        {!! link_to(url('feeds/'.$feed->id), 'Delete feed', ['class' => 'btn btn-danger btn-lg btn-block',
                                                                            'role' => 'button',
                                                                            'data-method' => 'delete',
                                                                            'data-confirm' => 'Are you sure?']) !!}
                        {!! link_to(url('feeds'), 'Back', ['class' => 'btn btn-default btn-lg btn-block']) !!}
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
@stop

@section('scripts')
    <script>
        $(document).ready(function() {
            deleteLink.initialize();
        });
    </script>
    <script>
        window.csrfToken = '<?php echo csrf_token(); ?>';
    </script>
@endsection