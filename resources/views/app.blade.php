<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <link href="{{ elixir('css/all.css') }}" rel="stylesheet" type="text/css" >
</head>
<body>

<div class="container">
    @include('flash::message')
    @yield('content')
</div>

@yield('footer')
<script src="{{ elixir('js/all.js') }}"></script>
<script>
    setTimeout("$('.alert-error').fadeOut('slow')", 15000);
    setTimeout("$('.alert-warning').fadeOut('slow')", 10000);
    setTimeout("$('.alert-info').fadeOut('slow')", 5000);
    setTimeout("$('.alert-success').fadeOut('slow')", 3000);
</script>
@yield('scripts')
</body>
</html>