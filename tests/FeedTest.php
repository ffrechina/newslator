<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FeedTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
        parent::setUp();

        $this->feeds = factory(App\Feed::class, 5)->create();
    }

    /** @test */
    public function it_verifies_that_feeds_are_correctly_loaded()
    {
        $feedsToLoad = \App\Feed::where('created_at', '>=', \Carbon\Carbon::today())->orderBy('created_at', 'desc');
        $paginateLimit = 5;
        $firstFeedToLoad = $feedsToLoad->paginate($paginateLimit)->items()[0];
        $secondFeedToLoad = $feedsToLoad->paginate($paginateLimit)->items()[1];
        $thirdFeedToLoad = $feedsToLoad->paginate($paginateLimit)->items()[2];
        $forthFeedToLoad = $feedsToLoad->paginate($paginateLimit)->items()[3];
        $fifthFeedToLoad = $feedsToLoad->paginate($paginateLimit)->items()[4];
        $totalFeeds = $feedsToLoad->count();
        $totalPages = (int) ceil($totalFeeds / $paginateLimit);

        $this->visit('/feeds')
            ->see('News')
            ->see(\Carbon\Carbon::today()->format('d-m-Y'))
            ->see($firstFeedToLoad->publisher)
            ->see($firstFeedToLoad->title)
            ->see($firstFeedToLoad->body)
            ->seeLink('View Article', $firstFeedToLoad->source)
            ->see($secondFeedToLoad->publisher)
            ->see($secondFeedToLoad->title)
            ->see($secondFeedToLoad->body)
            ->seeLink('View Article', $secondFeedToLoad->source)
            ->see($thirdFeedToLoad->publisher)
            ->see($thirdFeedToLoad->title)
            ->see($thirdFeedToLoad->body)
            ->seeLink('View Article', $thirdFeedToLoad->source)
            ->see($forthFeedToLoad->publisher)
            ->see($forthFeedToLoad->title)
            ->see($forthFeedToLoad->body)
            ->seeLink('View Article', $forthFeedToLoad->source)
            ->see($fifthFeedToLoad->publisher)
            ->see($fifthFeedToLoad->title)
            ->see($fifthFeedToLoad->body)
            ->seeLink('View Article', $fifthFeedToLoad->source)
            ->countElements('.feed-block', $paginateLimit)
            ->countElements('.pagination li', $totalFeeds > $paginateLimit ? $totalPages + 2 : 0)
            ->assertResponseOk();
    }


    /** @test */
    public function it_verifies_that_a_feed_can_be_created()
    {
        $feedToCreate = factory(App\Feed::class)->make();
        $this->visit('/feeds/create')
            ->type($feedToCreate->publisher, 'publisher')
            ->type($feedToCreate->title, 'title')
            ->type($feedToCreate->body, 'body')
            ->type($feedToCreate->source, 'source')
            ->press('Create')
            ->seeInDatabase('feeds', ['publisher' => $feedToCreate->publisher,
                                     'title' => $feedToCreate->title,
                                     'body' => $feedToCreate->body,
                                     'source' => $feedToCreate->source])
            ->see('Feed created correctly.')
            ->assertResponseOk();
    }


    /** @test */
    public function it_verifies_that_title_field_is_required_when_feed_created()
    {
        $feedToCreate = factory(App\Feed::class)->make();
        $this->visit('/feeds/create')
            ->type($feedToCreate->publisher, 'publisher')
            ->type($feedToCreate->body, 'body')
            ->type($feedToCreate->source, 'source')
            ->press('Create')
            ->seePageIs('/feeds/create')
            ->see('The title field is required.');
    }

    /** @test */
    public function it_verifies_that_body_field_is_required_when_feed_created()
    {
        $feedToCreate = factory(App\Feed::class)->make();
        $this->visit('/feeds/create')
            ->type($feedToCreate->publisher, 'publisher')
            ->type($feedToCreate->title, 'title')
            ->type($feedToCreate->source, 'source')
            ->press('Create')
            ->seePageIs('/feeds/create')
            ->see('The body field is required.');
    }

    /** @test */
    public function it_verifies_that_publisher_field_is_required_when_feed_created()
    {
        $feedToCreate = factory(App\Feed::class)->make();
        $this->visit('/feeds/create')
            ->type($feedToCreate->title, 'title')
            ->type($feedToCreate->body, 'body')
            ->type($feedToCreate->source, 'source')
            ->press('Create')
            ->seePageIs('/feeds/create')
            ->see('The publisher field is required.');
    }

    /** @test */
    public function it_verifies_that_a_feed_can_be_shown()
    {
        $feedToShow = $this->feeds->first();
        $this->visit('/feeds/' . $feedToShow->id)
            ->see('Show a Feed')
            ->see($feedToShow->publisher)
            ->see($feedToShow->title)
            ->see($feedToShow->body)
            ->seeLink('View Article', $feedToShow->source)
            ->seeLink('Edit feed')
            ->seeLink('Back')
            ->assertResponseOk();
    }

    /** @test */
    public function it_verifies_that_a_feed_can_be_edited()
    {
        $feedToEdit = $this->feeds->first();
        $feedForEditing = factory(App\Feed::class)->make();
        $this->visit('/feeds/' . $feedToEdit->id . '/edit')
            ->type($feedForEditing->publisher, 'publisher')
            ->type($feedForEditing->title, 'title')
            ->type($feedForEditing->body, 'body')
            ->type($feedForEditing->source, 'source')
            ->press('Update')
            ->see('Feed updated correctly.')
            ->seePageIs('/feeds/' . $feedToEdit->id)
            ->see('Show a Feed')
            ->see($feedForEditing->title)
            ->see($feedForEditing->body)
            ->assertResponseOk();
    }

    /** @test */
    public function it_verifies_that_a_feed_can_be_removed()
    {
        $feedToRemove = $this->feeds->first();
        $this->makeRequest('DELETE', '/feeds/' . $feedToRemove->id)
            ->dontSeeInDatabase('feeds', ['id' => $feedToRemove->id])
            ->seePageIs('/feeds/')
            ->see('Feed deleted correctly.')
            ->assertResponseOk();
    }

    public function tearDown()
    {
        foreach ($this->feeds as $feed) {
            $feed->image->destroy();
        }

        parent::tearDown();
    }
}
