<?php

use Goutte\Client;
use \App\Scrapers\NewsScraper;

class ScraperTest extends TestCase
{
    /** @test */
    public function it_verifies_that_the_elmundo_scraper_gets_correct_data()
    {
        $scraper = new \App\Scrapers\ElMundoScraper(new Client());

        $this->assertTitle($scraper);
        $this->assertBody($scraper);
        $this->assertSource($scraper);
        $this->assertImage($scraper);
    }

    /** @test */
    public function it_verifies_that_the_elconfidencial_scraper_gets_correct_data()
    {
        $scraper = new \App\Scrapers\ElConfidencialScraper(new Client());

        $this->assertTitle($scraper);
        $this->assertBody($scraper);
        $this->assertSource($scraper);
        $this->assertImage($scraper);
    }

    /** @test */
    public function it_verifies_that_the_elpais_scraper_gets_correct_data()
    {
        $scraper = new \App\Scrapers\ElPaisScraper(new Client());

        $this->assertTitle($scraper);
        $this->assertBody($scraper);
        $this->assertSource($scraper);
        $this->assertImage($scraper);
    }

    /** @test */
    public function it_verifies_that_the_elperiodico_scraper_gets_correct_data()
    {
        $scraper = new \App\Scrapers\ElPeriodicoScraper(new Client());

        $this->assertTitle($scraper);
        $this->assertBody($scraper);
        $this->assertSource($scraper);
        $this->assertImage($scraper);
    }

    /** @test */
    public function it_verifies_that_the_larazon_scraper_gets_correct_data()
    {
        $scraper = new \App\Scrapers\LaRazonScraper(new Client());

        $this->assertTitle($scraper);
        $this->assertBody($scraper);
        $this->assertSource($scraper);
        $this->assertImage($scraper);
    }

    /**
     * Asserts the title is not empty and is a string
     *
     * @param NewsScraper $scraper
     */
    private function assertTitle(NewsScraper $scraper)
    {
        $title = $scraper->scrapTitle();
        $this->assertNotEmpty($title);
        $this->assertTrue(is_string($title));
    }

    /**
     * Asserts the body is not empty and is a string
     *
     * @param NewsScraper $scraper
     */
    private function assertBody(NewsScraper $scraper)
    {
        $body = $scraper->scrapBody();
        $this->assertNotEmpty($body);
        $this->assertTrue(is_string($body));
    }

    /**
     * Asserts the source is not empty, is a string and has the correct format
     *
     * @param NewsScraper $scraper
     */
    private function assertSource(NewsScraper $scraper)
    {
        $source = $scraper->scrapSource();
        $this->assertNotEmpty($source);
        $this->assertTrue(is_string($source));
        $this->equalTo(preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', $source), 1);
    }

    /**
     * Asserts the image is not empty and has the correct format
     *
     * @param NewsScraper $scraper
     */
    private function assertImage(NewsScraper $scraper)
    {
        $image = $scraper->scrapImage();
        $this->assertNotEmpty($image);
        $this->equalTo(preg_match('/\/tmp\/(.*)(\.)(jpg|png|jpeg|gif)$/', $image), 1);
    }
}
