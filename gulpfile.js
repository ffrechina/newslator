var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
 mix.sass('app.scss', 'resources/assets/css');

 mix.styles([
  'libs/bootstrap.css',
  'app.css'
 ]);

 mix.scripts([
  'libs/jquery.js',
  'libs/bootstrap.js',
  'app.js'
 ]);

 mix.version([
  'public/css/all.css',
  'public/js/all.js'
 ]);

 mix.copy('resources/assets/fonts', 'public/build/fonts');
 mix.copy('resources/assets/images', 'public/build/images');

});
