<?php

use Illuminate\Database\Seeder;
use App\Feed;

class FeedTableSeeder extends Seeder
{
    /**
     * Run the database feed seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Feed::class, 5)->create();
    }
}
