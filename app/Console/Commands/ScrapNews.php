<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\Jobs\ElPaisScraperJob;
use \App\Jobs\ElMundoScraperJob;
use \App\Jobs\ElConfidencialScraperJob;
use \App\Jobs\LaRazonScraperJob;
use \App\Jobs\ElPeriodicoScraperJob;

class ScrapNews extends Command
{
    /**
     * The name and signature of ScrapNews command.
     *
     * @var string
     */
    protected $signature = 'scrapnews';

    /**
     * ScrapNews command description.
     *
     * @var string
     */
    protected $description = 'Scraps news from El Pais, El Mundo, La Razon, El Periodico and El Confidencial';

    /**
     * Create a new ScrapNews instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute ScrapNews command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new ElPaisScraperJob());
        dispatch(new ElMundoScraperJob());
        dispatch(new ElConfidencialScraperJob());
        dispatch(new LaRazonScraperJob());
        dispatch(new ElPeriodicoScraperJob());
    }
}
