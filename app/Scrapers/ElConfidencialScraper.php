<?php

namespace App\Scrapers;

use Goutte\Client;

class ElConfidencialScraper extends NewsScraper
{
    const URI = 'http://www.elconfidencial.com';

    private $mainCrawler;
    private $articleCrawler;
    private $headerCrawler;
    private $sourceCrawler;

    /**
     * ElConfidencialScraper constructor
     *
     * @param Client $goutteClient
     */
    public function __construct(Client $goutteClient)
    {
        $this->mainCrawler = $goutteClient->request('GET', $this::URI);
        $this->articleCrawler = $this->mainCrawler->filterXPath('//article[@data-mobile-order="1"]');
        $this->headerCrawler = $this->articleCrawler->filterXPath('//h3/a');
        $this->sourceCrawler = $goutteClient->click($this->headerCrawler->link());
    }

    /**
     * Get the title
     *
     * @return mixed
     */
    public function scrapTitle()
    {
        return $this->headerCrawler->text();
    }

    /**
     * Get the body
     *
     * @return mixed
     */
    public function scrapBody()
    {
        return $this->sourceCrawler->filterXPath('//div[@class="news-header-opening-txt"]')->text();
    }

    /**
     * Get the source
     *
     * @return mixed
     */
    public function scrapSource()
    {
        return $this->headerCrawler->attr('href');
    }

    /**
     * Get the image
     *
     * @return null|string
     */
    public function scrapImage()
    {
        $imageCrawler = $this->sourceCrawler->filterXPath('//figure/img');
        if ($imageCrawler->count()) {
            $imageUri = preg_replace('/(.*)(\..*)(\?.*)/', '${1}${2}', $imageCrawler->attr('src'));

            $fileName = md5(uniqid(empty($_SERVER['SERVER_ADDR']) ? '' : $_SERVER['SERVER_ADDR'], true))
                . '.' . pathinfo($imageUri)['extension'];
            $filePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;

            $success = copy($imageUri, $filePath);

            return $success ? $filePath : null;
        } else {
            return null;
        }
    }
}
