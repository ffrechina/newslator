<?php

namespace App\Scrapers;

abstract class NewsScraper
{
    abstract public function scrapTitle();
    abstract public function scrapBody();
    abstract public function scrapSource();
    abstract public function scrapImage();
}
