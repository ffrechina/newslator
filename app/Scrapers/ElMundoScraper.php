<?php

namespace App\Scrapers;

use Goutte\Client;

class ElMundoScraper extends NewsScraper
{
    const URI = 'http://www.elmundo.es';

    private $mainCrawler;
    private $articleCrawler;
    private $headerCrawler;
    private $sourceCrawler;

    /**
     * ElMundoScraper constructor
     *
     * @param Client $goutteClient
     */
    public function __construct(Client $goutteClient)
    {
        $this->mainCrawler = $goutteClient->request('GET', $this::URI);
        $this->articleCrawler = $this->getMainArticleCrawler();
        $this->headerCrawler = $this->articleCrawler->filterXPath('//header/h3/a');
        $this->sourceCrawler = $goutteClient->click($this->headerCrawler->link());
    }

    /**
     * Get the main article crawler
     *
     * @return mixed
     */
    private function getMainArticleCrawler()
    {
        $mainArticle = $this->mainCrawler->filterXPath('//article[1]');
        $articleCount = $this->mainCrawler->filterXPath('//article')->count();
        $figureCount = $mainArticle->filterXPath('//figure/a/img')->count();
        if (!$figureCount) {
            for ($i = 2; $i < $articleCount; $i++) {
                $mainArticle = $this->mainCrawler->filterXPath('//article[' . $i . ']');
                $figureCount = $mainArticle->filterXPath('//figure/a/img')->count();
                if ($figureCount) {
                    break;
                }
            }
        }
        return $mainArticle;
    }

    /**
     * Get the title
     *
     * @return mixed
     */
    public function scrapTitle()
    {
        return $this->headerCrawler->text();
    }

    /**
     * Get the body
     *
     * @return mixed
     */
    public function scrapBody()
    {
        $sourceArticleCrawler = $this->sourceCrawler->filterXPath('//article[1]');
        if ($sourceArticleCrawler->attr('class') == 'desarrollo') {
            return $sourceArticleCrawler->filterXPath('//div[@class="entradilla"]/p')->text();
        } else {
            return $sourceArticleCrawler->filterXPath('//div/time/following-sibling::p')->text();
        }
    }

    /**
     * Get the source
     *
     * @return mixed
     */
    public function scrapSource()
    {
        return $this->headerCrawler->attr('href');
    }

    /**
     * Get the image
     *
     * @return null|string
     */
    public function scrapImage()
    {
        $imageCrawler = $this->articleCrawler->filterXPath('//figure/a/img');
        if ($imageCrawler->count()) {
            $imageUri = 'http:' . $imageCrawler->attr('src');

            $fileName = md5(uniqid(empty($_SERVER['SERVER_ADDR']) ? '' : $_SERVER['SERVER_ADDR'], true))
                . '.' . pathinfo($imageUri)['extension'];
            $filePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;

            $success = copy($imageUri, $filePath);

            return $success ? $filePath : null;
        } else {
            return null;
        }
    }
}
