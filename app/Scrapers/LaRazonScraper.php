<?php

namespace App\Scrapers;

use Goutte\Client;

class LaRazonScraper extends NewsScraper
{
    const URI = 'http://www.larazon.es';

    private $mainCrawler;
    private $articleCrawler;
    private $headerCrawler;

    /**
     * LaRazonScraper constructor
     *
     * @param Client $goutteClient
     */
    public function __construct(Client $goutteClient)
    {
        $this->mainCrawler = $goutteClient->request('GET', $this::URI);
        $this->articleCrawler = $this->mainCrawler->filterXPath('//div[contains(@class,"teaserPrincipal")]')->first();
        $this->headerCrawler = $this->articleCrawler->filterXPath('//h2/a');
    }

    /**
     * Get the title
     *
     * @return mixed
     */
    public function scrapTitle()
    {
        return $this->headerCrawler->attr('title');
    }

    /**
     * Get the body
     *
     * @return mixed
     */
    public function scrapBody()
    {
        return $this->articleCrawler->filterXPath('//div[@class="teaser"]//p')->text();
    }

    /**
     * Get the source
     *
     * @return mixed
     */
    public function scrapSource()
    {
        return $this::URI . $this->headerCrawler->attr('href');
    }

    /**
     * Get the image
     *
     * @return null|string
     */
    public function scrapImage()
    {
        $imageCrawler = $this->articleCrawler->filterXPath('//img');

        if ($imageCrawler->count()) {
            if (preg_match('/.*transparentPixel.*/', $imageCrawler->attr('src'))) {
                $imageUri = $this::URI . $imageCrawler->attr('data-original');
            } else {
                $imageUri = $this::URI . $imageCrawler->attr('src');
            }

            $fileName = md5(uniqid(empty($_SERVER['SERVER_ADDR']) ? '' : $_SERVER['SERVER_ADDR'], true))
                . '.' . pathinfo($imageUri)['extension'];
            $filePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;

            $success = copy($imageUri, $filePath);

            return $success ? $filePath : null;
        } else {
            return null;
        }
    }
}
