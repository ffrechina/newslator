<?php

namespace App\Scrapers;

use Goutte\Client;

class ElPeriodicoScraper extends NewsScraper
{
    const URI = 'http://www.elperiodico.com';

    private $mainCrawler;
    private $articleCrawler;
    private $headerCrawler;
    private $sourceCrawler;

    /**
     * ElPeriodicoScraper constructor
     *
     * @param Client $goutteClient
     */
    public function __construct(Client $goutteClient)
    {
        $this->mainCrawler = $goutteClient->request('GET', $this::URI);
        $this->articleCrawler = $this->mainCrawler->filterXPath('//div[contains(@class, "comp ep-noticia")]')->first();
        $this->headerCrawler = $this->articleCrawler->filterXPath('//h2/a');
        $this->sourceCrawler = $goutteClient->click($this->headerCrawler->link());
    }

    /**
     * Get the title
     *
     * @return mixed
     */
    public function scrapTitle()
    {
        return $this->headerCrawler->text();
    }

    /**
     * Get the body
     *
     * @return mixed
     */
    public function scrapBody()
    {
        return $this->sourceCrawler->filterXPath('//div[@class="comp cuerpo-noticia"]/p')->eq(1)->text();
    }

    /**
     * Get the source
     *
     * @return mixed
     */
    public function scrapSource()
    {
        return $this::URI . $this->headerCrawler->attr('href');
    }

    /**
     * Get the image
     *
     * @return null|string
     */
    public function scrapImage()
    {
        $imageCrawler = $this->sourceCrawler->filterXPath('//div[@class="unit c100 last"]//img')->first();

        if ($imageCrawler->count() == 0) {
            $imageVideoCrawler = $this->sourceCrawler->filterXPath('//div[@class="comp ep-video"]//img');
            if ($imageVideoCrawler->count() == 0) {
                return null;
            } else {
                $imageUri = $imageVideoCrawler->attr('src');
            }
        } else {
            $imageUri = $imageCrawler->attr('src');
        }

        $fileName = md5(uniqid(empty($_SERVER['SERVER_ADDR']) ? '' : $_SERVER['SERVER_ADDR'], true))
            . '.' . pathinfo($imageUri)['extension'];
        $filePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;

        $success = copy($imageUri, $filePath);

        return $success ? $filePath : null;
    }
}
