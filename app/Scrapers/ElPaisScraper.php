<?php

namespace App\Scrapers;

use Goutte\Client;

class ElPaisScraper extends NewsScraper
{
    const URI = 'http://www.elpais.com';

    private $mainCrawler;
    private $articleCrawler;
    private $headerCrawler;
    private $sourceCrawler;

    /**
     * ElPaisScraper constructor
     *
     * @param Client $goutteClient
     */
    public function __construct(Client $goutteClient)
    {
        $this->mainCrawler = $goutteClient->request('GET', $this::URI);
        $this->articleCrawler = $this->mainCrawler->filterXPath('//div[contains(@class, "article destacada")]');
        $this->headerCrawler = $this->articleCrawler->filterXPath('//h2/a');
        $this->sourceCrawler = $goutteClient->click($this->headerCrawler->link());
    }

    /**
     * Get the title
     *
     * @return mixed
     */
    public function scrapTitle()
    {
        return $this->headerCrawler->text();
    }

    /**
     * Get the body
     *
     * @return mixed
     */
    public function scrapBody()
    {
        return $this->articleCrawler->filterXPath('//p')->text();
    }

    /**
     * Get the source
     *
     * @return mixed
     */
    public function scrapSource()
    {
        return $this::URI . $this->headerCrawler->attr('href');
    }

    /**
     * Get the image if exists, null otherwise
     *
     * @return null|string
     */
    public function scrapImage()
    {
        $imageCrawler = $this->sourceCrawler->filterXPath('//div[@id="articulo_contenedor"]//img');

        if ($imageCrawler->count() == 0) {
            $imageUriFromEmbeddedVideo = $this->extractImageUriFromEmbeddedVideo(
                                    $this->sourceCrawler->filterXPath('//div[@id="articulo_contenedor"]//script'));

            if ($imageUriFromEmbeddedVideo) {
                $imageUri = $this::URI . $imageUriFromEmbeddedVideo;
            } else {
                return null;
            }
        } else {
            $imageUri = $imageCrawler->attr('src');
        }

        $fileName = md5(uniqid(empty($_SERVER['SERVER_ADDR']) ? '' : $_SERVER['SERVER_ADDR'], true))
            . '.' . pathinfo($imageUri)['extension'];
        $filePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $fileName;

        $success = copy($imageUri, $filePath);

        return $success ? $filePath : null;
    }

    /**
     * Get the preview image from the embedded video
     *
     * @param $videoCrawler
     *
     * @return string|null
     */
    private function extractImageUriFromEmbeddedVideo($videoCrawler)
    {
        if (preg_match_all('/urlFotogramaFijo.*\'(.*)\'.*/m', $videoCrawler->html(), $output)) {
            return $output[1][0];
        } else {
            return null;
        }
    }
}
