<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->deleteFeedImageBeforeDeleting();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Deletes the feed's image before deleting.
     *
     */
    private function deleteFeedImageBeforeDeleting()
    {
        \App\Feed::deleting(function ($feed) {
            if ($feed->image) {
                $feed->image->delete();
            }
        });
    }
}
