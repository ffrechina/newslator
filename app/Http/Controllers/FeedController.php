<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedRequest;
use App\Http\Controllers\Controller;
use \App\Feed;
use \Flash;
use \Carbon\Carbon;

class FeedController extends Controller
{
    /**
     * Display a listing of feeds.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeds = Feed::where('created_at', '>=', Carbon::today())->orderBy('created_at', 'desc')->paginate(5);

        return view('feeds.index', compact('feeds'));
    }

    /**
     * Show the form for creating a new feed.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('feeds.create');
    }

    /**
     * Store a newly created feed in storage.
     *
     * @param  FeedRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(FeedRequest $request)
    {
        $feed = Feed::create($request->all());

        Flash::success('Feed created correctly.');

        return redirect('feeds/'.$feed->id);
    }

    /**
     * Display the specified feed.
     *
     * @param Feed $feed
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Feed $feed)
    {
        return view('feeds.show', compact('feed'));
    }

    /**
     * Show the form for editing the specified feed.
     *
     * @param Feed $feed
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Feed $feed)
    {
        return view('feeds.edit', compact('feed'));
    }

    /**
     * Update the specified feed in storage.
     *
     * @param  FeedRequest $request
     * @param Feed $feed
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(FeedRequest $request, Feed $feed)
    {
        $feed->update($request->all());

        Flash::success('Feed updated correctly.');

        return redirect('feeds/'.$feed->id);
    }

    /**
     * Remove the specified feed from storage.
     *
     * @param Feed $feed
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Feed $feed)
    {
        Feed::destroy($feed->id);

        Flash::success('Feed deleted correctly.');

        return redirect('feeds');
    }
}
