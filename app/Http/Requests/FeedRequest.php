<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FeedRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the feed's validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  => 'required|max:255',
            'body'  => 'required',
            'image'  => 'image|max:10000',
            'source'  => 'required|url|max:255',
            'publisher'  => 'required|max:255',
        ];
    }
}
