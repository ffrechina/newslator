<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Jenssegers\Mongodb\Eloquent\Model;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

/**
 * App\Feed
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property string $source
 * @property string $publisher
 * @property string $image_file_name
 * @property integer $image_file_size
 * @property string $image_content_type
 * @property string $image_updated_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Feed extends Model implements StaplerableInterface
{
    use EloquentTrait;

    /**
     * Attributes mass-assignable
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'body',
        'image',
        'source',
        'publisher'
    ];

    /**
     * Validation rules for Feed
     *
     * @var array
     */
    public static $validation_rules = ['title'      => 'required|max:255',
                                       'body'       => 'required',
                                       'image'      => ['regex:/\/tmp\/(.*)(\.)(jpg|png|jpeg|gif)$/'],
                                       'source'     => 'required|url|max:255',
                                       'publisher'  => 'max:255',];
    /**
     * Feed constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->hasAttachedFile('image', [
            'styles' => [
                'large' => '700x300',
                'medium' => '350x150'
            ],
            'default_url' => '/build/:attachment/:style/missing.png'
        ]);

        parent::__construct($attributes);
    }
}
