<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Feed;
use App\Scrapers\LaRazonScraper;
use Validator;

class LaRazonScraperJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Execute the job.
     *
     * @param LaRazonScraper $laRazonScraper
     *
     * @throws \Exception
     */
    public function handle(LaRazonScraper $laRazonScraper)
    {
        $title = $laRazonScraper->scrapTitle();
        $body = $laRazonScraper->scrapBody();
        $source = $laRazonScraper->scrapSource();
        $image = $laRazonScraper->scrapImage();

        $validator =
            Validator::make(['title'  => $title, 'body' => $body, 'image'  => $image, 'source' => $source],
                Feed::$validation_rules);

        if ($validator->fails()) {
            throw new \Exception('LaRazonScraper failed: ' . $validator->errors());
        } else {
            Feed::create(['title' => $title, 'body' => $body, 'image' => $image,
                          'source' => $source, 'publisher' => 'La Razon']);
        }
    }
}
