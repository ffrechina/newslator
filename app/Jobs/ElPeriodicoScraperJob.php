<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Feed;
use App\Scrapers\ElPeriodicoScraper;
use Validator;

class ElPeriodicoScraperJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Execute the ElPeriodicoScraperJob.
     *
     * @param ElPeriodicoScraper $elPeriodicoScraper
     *
     * @throws \Exception
     */
    public function handle(ElPeriodicoScraper $elPeriodicoScraper)
    {
        $title = $elPeriodicoScraper->scrapTitle();
        $body = $elPeriodicoScraper->scrapBody();
        $source = $elPeriodicoScraper->scrapSource();
        $image = $elPeriodicoScraper->scrapImage();

        $validator =
            Validator::make(['title'  => $title, 'body' => $body, 'image'  => $image, 'source' => $source],
                Feed::$validation_rules);

        if ($validator->fails()) {
            throw new \Exception('ElPeriodicoScraper failed: ' . $validator->errors());
        } else {
            Feed::create(['title' => $title, 'body' => $body, 'image' => $image,
                          'source' => $source, 'publisher' => 'El Periodico']);
        }
    }
}
