<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Feed;
use App\Scrapers\ElPaisScraper;
use Validator;

class ElPaisScraperJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Execute the ElPaisScraperJob.
     *
     * @param ElPaisScraper $elPaisScraper
     *
     * @throws \Exception
     */
    public function handle(ElPaisScraper $elPaisScraper)
    {
        $title = $elPaisScraper->scrapTitle();
        $body = $elPaisScraper->scrapBody();
        $source = $elPaisScraper->scrapSource();
        $image = $elPaisScraper->scrapImage();

        $validator =
            Validator::make(['title'  => $title, 'body' => $body, 'image'  => $image, 'source' => $source],
                Feed::$validation_rules);

        if ($validator->fails()) {
            throw new \Exception('ElPaisScraper failed: ' . $validator->errors());
        } else {
            Feed::create(['title' => $title, 'body' => $body, 'image' => $image,
                          'source' => $source, 'publisher' => 'El Pais']);
        }
    }
}
