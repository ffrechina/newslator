<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Feed;
use App\Scrapers\ElConfidencialScraper;
use Validator;

class ElConfidencialScraperJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Executes the ElConfidencialScraperJob.
     *
     * @param ElConfidencialScraper $elConfidencialScraper
     *
     * @throws \Exception
     */
    public function handle(ElConfidencialScraper $elConfidencialScraper)
    {
        $title = $elConfidencialScraper->scrapTitle();
        $body = $elConfidencialScraper->scrapBody();
        $source = $elConfidencialScraper->scrapSource();
        $image = $elConfidencialScraper->scrapImage();

        $validator =
            Validator::make(['title'  => $title, 'body' => $body, 'image'  => $image, 'source' => $source],
                        Feed::$validation_rules);

        if ($validator->fails()) {
            throw new \Exception('ElConfidencialScraper failed: ' . $validator->errors());
        } else {
            Feed::create(['title' => $title, 'body' => $body, 'image' => $image,
                          'source' => $source, 'publisher' => 'El Confidencial']);
        }
    }
}
