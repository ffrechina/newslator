<?php

namespace App\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Feed;
use App\Scrapers\ElMundoScraper;
use Validator;

class ElMundoScraperJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Execute the ElMundoScraperJob.
     *
     * @param ElMundoScraper $elMundoScraper
     *
     * @throws \Exception
     */
    public function handle(ElMundoScraper $elMundoScraper)
    {
        $title = $elMundoScraper->scrapTitle();
        $body = $elMundoScraper->scrapBody();
        $source = $elMundoScraper->scrapSource();
        $image = $elMundoScraper->scrapImage();

        $validator =
            Validator::make(['title'  => $title, 'body' => $body, 'image'  => $image, 'source' => $source],
                Feed::$validation_rules);

        if ($validator->fails()) {
            throw new \Exception('ElMundoScraper failed: ' . $validator->errors());
        } else {
            Feed::create(['title' => $title, 'body' => $body, 'image' => $image,
                          'source' => $source, 'publisher' => 'El Mundo']);
        }
    }
}
